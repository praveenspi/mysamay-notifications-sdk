import { ContextVariable } from "@neb-sports/mysamay-common-utils";

export class URLHelper {
    baseurl: string;
    contextPath: string = "notification-srv";
    sendsms: string = this.contextPath + "/sms";
    sendpush: string = this.contextPath + "/push";
    sendemail: string = this.contextPath + "/email";
    savePush: string = this.contextPath + "/push/sent";
    saveRecipient: string = this.contextPath + "/push/recipients";

    constructor(contextVar: ContextVariable) {
        this.baseurl = contextVar.baseUrl;
    }

}