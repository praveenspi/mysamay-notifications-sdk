export interface PushRequest {
    data?: any;
    notification?: any;
    to?: string[];
}