export interface EmailRequest {
    to: string[];
    body: string;
    subject: string;
}