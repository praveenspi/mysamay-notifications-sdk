export interface SMSRequest {
    to: string;
    body: string;
}