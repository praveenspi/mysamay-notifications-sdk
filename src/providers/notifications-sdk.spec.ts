import { HttpHelper } from '@neb-sports/mysamay-common-utils';
import { ContextVariable } from '@neb-sports/mysamay-common-utils';
import { MetadataHelper } from '@neb-sports/mysamay-common-utils';
import { Test, TestingModule } from '@nestjs/testing';

import { NotificationsSDK } from "./notifications-sdk";



describe('NotificationssSDK', () => {
    let sdk: NotificationsSDK;


    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [],
            providers: [NotificationsSDK],
        }).compile();

        sdk = module.get<NotificationsSDK>(NotificationsSDK);
    });

    it('should be defined', () => {
        expect(sdk).toBeDefined();
    });

});
