import { SMSRequest } from './../entities/sms-request';
import { URLHelper } from '../helpers/url-helper';
import { Injectable } from "@nestjs/common";

import urljoin from "url-join";
import { HttpHelper, MetadataHelper, ResponseEntity } from "@neb-sports/mysamay-common-utils";
import { PushRequest } from '../entities/push-request';
import { EmailRequest } from '../entities/email-request';
import { PushNotification, PushRecipient } from "@neb-sports/mysamay-notification-model";

@Injectable()
export class NotificationsSDK {

    urlHelper: URLHelper;

    constructor() {
    }

    init() {
        if (!this.urlHelper) {
            this.urlHelper = new URLHelper(MetadataHelper.getContextVariable());
        }
    }


    async sendPush(data: PushRequest): Promise<ResponseEntity<string[]>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.sendpush);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.postRequest<ResponseEntity<string[]>>(url, data, headers);
            return resp.body;
        }
        catch(error) {
            return error.response;
        }  
    }

    async savePush(data: PushNotification[]): Promise<ResponseEntity<any>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.savePush);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.postRequest<ResponseEntity<any>>(url, data, headers);
            return resp.body;
        }
        catch(error) {
            return error.response;
        }   
    }

    async savePushRecipients(data: PushRecipient[]): Promise<ResponseEntity<any>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.saveRecipient);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.postRequest<ResponseEntity<any>>(url, data, headers);
            return resp.body;
        }
        catch(error) {
            return error.response;
        }   
    }

    async sendEmail(data: EmailRequest): Promise<ResponseEntity<any>> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.sendemail);
        let headers = this.prepareHeader();
        let resp = await HttpHelper.postRequest<ResponseEntity<any>>(url, data, headers);
        return resp.body;
    }

    async sendSMS(data: SMSRequest): Promise<ResponseEntity<any>> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.sendsms);
        let headers = this.prepareHeader();
        let resp = await HttpHelper.postRequest<ResponseEntity<any>>(url, data, headers);
        return resp.body;
    }

    prepareHeader() {
        let contextVar = MetadataHelper.getContextVariable();
        return {
            "X-Access-Token": contextVar.tokenHash,
            "X-Ref-Number": contextVar.refNumber
        }
    }

}
