import { NotificationsSDK } from '../providers/notifications-sdk';
import { Module } from '@nestjs/common';

@Module({
    imports: [],
    providers: [
      NotificationsSDK
    ],
    exports: [
      NotificationsSDK
    ]
  })
export class NotificationsSDKModule {}
